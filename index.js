// console.log("Hello World");

// Repetition Control Structure (Loops)
	// Loops are one of the mos important feature that programming must have.
	// It lets us execute code repeatedly in a pre-set number or maybe forever.

/*
	
	Mini Activity:

	Create a function named greeting() and display a "Hello My World!" using console.log inside the function.

	Invoke the greeting() function 10 times.

	Take a screenshot of your work and send it to the batch hangouts.

*/

	function greeting(){
		console.log("Hello My World!");
	}

	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();
	// greeting();

	// let count = 10;

	// while(count !== 0){
	// 	console.log("This is printed inside the loop: " +count);
	// 	greeting();

	// 	// iteration
	// 	count--;
	// }

// [SECTION] While Loop
	// While loop allow us to repeat an action or an instruction as long as the condition is true.
	/*
		Syntax:
			while(expression/condition){
				//statement/code block

				finalExpression ++/-- (iteration)
			}

			- expression/condition => This are the unit of code that is being evaluated in our loop. Loop will run while the condition/expression is true. 
			- statement/code block => code/instructions that will be executed several times.
			- finalExpression => indicates how to advance the loop.
	*/

	// let count = 5;

	// // while the value of count is not equal to 0.
	// while(count !== 0){

	// 	console.log("Current value of count in the condition: " + count + " !== 0 is " +(count !==0));

	// 	// console.log("While: " +count);

	// 	// Decreases the value of count by 1 after every iteration of our loop to stop it when it reaches to 0.
	// 	// Forgetting to include this in loop will make our appliocation run an infinite loop which will eventually crash our devices.
	// 	count--;
	// }

// [SECTION] Do While Loop
	// A do-while loop works a lot like the while loop. But unlike while loopps do-while loops guarantee that the code will be excuted at least once.

	/*
		Syntax
			do{
				//statement/code block

				finalExpression ++/--
			} while(expression/condition)

	*/ 
			// Number() can also covert a boolean or date.
	// let number = Number(prompt("Give me a number"));

	// do {
	// 	console.log("Do While: " +number);

	// 	//increase the value of number by 1 after every iteration to stop when reaches 10 or grater.
	// 	number += 1;
	// //Providing a number of 10 or greater will run the code block once and will stop the loop
	// } while (number < 10)

	// while(number < 10){
	// 	console.log("While: " +number);

	// 	number++
	// }


// [SECTION] For Loop
	// A for loop is more flexible than while and do-while loops.

	/*
		Syntax:
			for(initialization; expression/condition; finalExpression++/--){
				// statement/code block
			}

			Three parts of the for loop:
			1. initialization => value that will track the progession of the loop.
			2. expression/condition => this will be evaluated to determine if the loop will run one more time.
			3. finalExpression => indicates how the loop advances. 
	*/
	/*
		Mini Activity

			Refactor the code below that the loop will only print the even numbers.

			Take a screenshot of your work and send it to the batch hangouts.

	*/
	// for(let count = 0; count <= 20; count++){
	// 	// console.log("For Loop: " +count);

	// 	if(count % 2 == 0 && count != 0){
	// 		console.log("Even: " +count);
	// 	}

	// }

	/*
		let myString = "angelito"

		expected output:
		a
		n
		g
		e
		l
		i
		t
		o
	*/

	// Loop using String property
	let myString = "angelito";

	// .length property is used to count the number of characters in a string.
	console.log(myString.length);

	// Accessing element of a string
	// Individual characters of a string may be accessed using it's index number.
	// The first character in string corresponds to the number 0, to the nth number.
	// console.log(myString[0]); //to access the starting element
	// console.log(myString[3]); //e
	// console.log(myString[myString.length-1]); //to access the last element of the string

	// Create the loop that will display the individual letters of the myString variable in the console.

	// for(let x = 0; x < myString.length; x++){
	// 	console.log(myString[x]);
	// }

	/*
		Creates a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is  a vowel.
	*/

	let myName = "TolIts"; //expect output: T3l3ts
	let myNewName = ""

	// for (let i = 0; i < myName.length; i++){
	// 	// console.log(myName[i].toLowerCase());

	// 	// If the character of your name is a vowel letter, instead of the character, it will display the number "3"
	// 	if(myName[i].toLowerCase() == 'a' || 
	// 		myName[i].toLowerCase() == 'e' || 
	// 		myName[i].toLowerCase() == 'i' || 
	// 		myName[i].toLowerCase() == 'o' || 
	// 		myName[i].toLowerCase() == 'u'){
	// 		//vowels
	// 		console.log(3);
	// 		myNewName += 3;
	// 	}
	// 	else{
	// 		//Consonants
	// 		console.log(myName[i]);
	// 		myNewName += myName[i];
	// 	}
	// }

	// //concatenates the characters based on the given condition
	// console.log(myNewName);

// [SECTION] Continue and Break Statements

/*
	- "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the code block.
	- "break" statement is used to terminate the current loop once a match has been found.
*/
/*
	Create a loop that if the count values is divided by 2 and the remainder is 0, it will not print the number and continue to the next iteration of the loop. if the count value is greater than 10 it will stop the loop.

*/

	// for(let count = 0; count <= 20; count++){
	// 	// console.log(count);
	// 		//10
	// 	if(count % 2 === 0){
	// 		continue;
	// 		// This ignores all statements located after the continue statement;
	// 		// console.log("Even: " +count);
	// 	}
	// 		console.log("Continue and Break: " +count);

	// 	if(count > 10){
	// 		break;
	// 	}
	// }

	/*

		Mini Activty:
		Creates a loop that will iterate based on the length of the string. If the current letter is equivalent to "a", print "Continue to the next iteration" and skip the current letter. If the current letter is equal to "d" stop the loop.

		let name = "Alexandro";
		
		Expected Output:

		Continue to the next iteration
		l
		e
		x
		continue to the next iteration
		n
		d
	*/

	let name = "Alexandro";

	for(let i = 0; i < name.length; i++){

		//If the letter is equal to a, continue to the next iteration of the loop.
		if(name[i].toLowerCase() === 'a'){
			console.log("Continue to the next iteration.");
			continue;
		}
		
		// The current letter will be printed out based on it's index (i).
		console.log(name[i]);

		// If the current letter is equal to "d", stop the loop
		if(name[i].toLowerCase() == 'd'){
			break;
		}
	}